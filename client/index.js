import React from 'react';
import ReactDOM from 'react-dom';
import User from './components/users.jsx';

ReactDOM.render(<User />, document.getElementById('user-app'));