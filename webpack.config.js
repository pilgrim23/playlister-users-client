const path = require('path');
const webpack = require('webpack');


const ENTRY_FILE = path.join(__dirname, 'client', 'index.js');

module.exports = {
  mode: 'development',
  entry: ENTRY_FILE,

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: "users.js"
  },

  plugins: [new webpack.ProgressPlugin()],

  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      include: [path.resolve(__dirname, 'client')],
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /.css$/,

      use: [{
        loader: "style-loader"
      }, {
        loader: "css-loader",

        options: {
          sourceMap: true
        }
      }]
    }]
  }
}